package main;

import ini.ProfileReader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import observer.Observateur;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

import org.eclipse.wb.swt.SWTResourceManager;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import socket.CupptSocket;
import thread.ObservableListener;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class Form1 {

	protected Shell shlCupptSdk;
	protected Display display;

	private Text textCUPPSPLT, textCUPPSPN, textCUPPSOPR, textCUPPSCN, textCUPPSPP, textXSDU,
			textCUPPSUN, textCUPPSACN;

	private StyledText styledTextGoals, styledTextXML, styledTextEvent, styledTextXMLValidate;

	private TabFolder tabFolder;
	private TabItem tabPageLesson1_2_3;

	private Table tableEvents, tableDeviceAcquireList;

	private Text textDeviceToken;
	private Label lblDeviceToken;

	private TableColumn tableColumn;
	private TableColumn tableColumn_1;
	private TableColumn tableColumn_2;
	private TableColumn tableColumn_3;
	private TableColumn tableColumn_4;
	private TableColumn tableColumn_5;
	private TableColumn tableColumn_6;
	private TableColumn tableColumn_7;
	private TableColumn tableColumn_8;
	private Text textHostName;
	private Text textHostIP;
	private Text textHostPort;
	private TabItem tabPageLesson20;
	private Button btnByeRequest;
	private StyledText styledTextByeResponse;
	private Label labelByeResponse;
	private Button btnDisconnect;
	private TabItem tabPageLesson4_5;
	private Composite compositeLesson4_5;
	private Label label_1;
	private Button btnDeviceAcquireRequest;
	private Label lblNewLabel_2;
	private Label lblGreenReady;
	private Label lblOrangePaperjam;
	private Label lblRed;

	private Combo comboInterfaceMode;

	private TabItem tabPageLesson6_7;
	private Label label_2;
	private Table tableConnectedDeviceList;
	private TableColumn tableColumn_13;
	private TableColumn tableColumn_16;
	private TableColumn tableColumn_17;
	private TableColumn tableColumn_18;
	private Button btnInterfaceModeRequest;
	private TableColumn tblclmnMode;
	private Label lblIniInterfaceLevel;
	private Text textIniInterfaceLevel;

	private Button btnAutoConnect, btnDeviceLockRequest;

	private TabItem tabPageLesson19;
	private Composite compositeLesson19;
	private Label lblApplicationStopRequest;
	private Table tableApplicationStopCommandRequest;
	private TableColumn tblclmnTime;
	private TableColumn tblclmnMessage;
	private TableColumn tblclmnCanDefer;

	//

	private String AeaRequestType = "";
	private String aeaText = "";
	private String aeaBinary = "";

	private int sequence;

	// Sockets
	private CupptSocket platformSocket = new CupptSocket();

	private ArrayList<CupptSocket> deviceSocketList = new ArrayList<CupptSocket>();

	// Vars
	String AirlineCode = "FR";

	String ApplicationCode = "EAS2012CUPPT0007";
	String ApplicationName = "Sample_DCS-J19";
	String ApplicationVersion = "01.03";
	String ApplicationData;

	String platformMessageID;

	String DeviceToken;
	// String DeviceName;

	// XSD informations
	String InterfaceLevel;
	String XSDLocalPath;

	String XSDVersion;

	// colors
	Color green = SWTResourceManager.getColor(50, 205, 50);
	Color orange = SWTResourceManager.getColor(255, 165, 0);
	Color red = SWTResourceManager.getColor(SWT.COLOR_RED);

	// application auto connection
	boolean autoconnect = false;
	private Button btnCuptestInstancesRequest;

	private String applicationStopCommandResponseResult;

	//

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Form1 window = new Form1();
			window.open();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCupptSdk.open();
		shlCupptSdk.layout();
		while (!shlCupptSdk.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();

			}
		}

	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCupptSdk = new Shell();
		shlCupptSdk.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				System.out.println("closing...");
				disconnect();

			}
		});

		shlCupptSdk.setImage(SWTResourceManager.getImage(Form1.class, "/main/icon53.png"));
		shlCupptSdk.setMinimumSize(new Point(971, 649));
		shlCupptSdk.setSize(1024, 649);
		shlCupptSdk.setText("CUPPT SDK - lesson 19 Java");

		Group grpPlatformConnection = new Group(shlCupptSdk, SWT.NONE);
		grpPlatformConnection.setText("Platform Connection");
		grpPlatformConnection.setBounds(10, 10, 988, 105);

		Label lblSitecuppsplt = new Label(grpPlatformConnection, SWT.NONE);
		lblSitecuppsplt.setBounds(10, 22, 101, 15);
		lblSitecuppsplt.setText("Site (CUPPSPLT)");

		textCUPPSPLT = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPLT.setBounds(117, 19, 89, 21);

		Label lblNodecuppspn = new Label(grpPlatformConnection, SWT.NONE);
		lblNodecuppspn.setText("Node (CUPPSPN)");
		lblNodecuppspn.setBounds(212, 22, 101, 15);

		textCUPPSPN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPN.setBounds(319, 19, 89, 21);

		Label lblOpratorcuppsopr = new Label(grpPlatformConnection, SWT.NONE);
		lblOpratorcuppsopr.setText("Operator (CUPPSOPR)");
		lblOpratorcuppsopr.setBounds(454, 22, 123, 15);

		textCUPPSOPR = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSOPR.setBounds(583, 19, 89, 21);

		Label lblComputerNamecuppscn = new Label(grpPlatformConnection, SWT.NONE);
		lblComputerNamecuppscn.setText("Computer Name (CUPPSCN)");
		lblComputerNamecuppscn.setBounds(704, 22, 160, 15);

		textCUPPSCN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSCN.setBounds(870, 19, 108, 21);

		Label lblPortcuppspp = new Label(grpPlatformConnection, SWT.NONE);
		lblPortcuppspp.setText("Port (CUPPSPP)");
		lblPortcuppspp.setBounds(212, 49, 101, 15);

		textCUPPSPP = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPP.setBounds(319, 46, 89, 21);

		Label lblXsdSchema = new Label(grpPlatformConnection, SWT.NONE);
		lblXsdSchema.setText("XSD");
		lblXsdSchema.setBounds(212, 77, 101, 15);

		textXSDU = new Text(grpPlatformConnection, SWT.BORDER);
		textXSDU.setBounds(319, 74, 353, 21);

		Label lblUsernamecuppsun = new Label(grpPlatformConnection, SWT.NONE);
		lblUsernamecuppsun.setText("UserName (CUPPSUN)");
		lblUsernamecuppsun.setBounds(454, 49, 123, 15);

		textCUPPSUN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSUN.setBounds(583, 46, 89, 21);

		Label lblAlternateNamecuppsacn = new Label(grpPlatformConnection, SWT.NONE);
		lblAlternateNamecuppsacn.setText("Alternate Name (CUPPSACN)");
		lblAlternateNamecuppsacn.setBounds(704, 49, 160, 15);

		textCUPPSACN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSACN.setBounds(870, 46, 108, 21);

		btnCuptestInstancesRequest = new Button(grpPlatformConnection, SWT.NONE);
		btnCuptestInstancesRequest.setEnabled(false);
		btnCuptestInstancesRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnCuptestInstancesRequest_Click(e);
			}
		});
		btnCuptestInstancesRequest.setText("cuptestInstancesRequest");
		btnCuptestInstancesRequest.setBounds(781, 72, 197, 25);

		tabFolder = new TabFolder(shlCupptSdk, SWT.NONE);
		tabFolder.setBounds(10, 121, 711, 336);

		tabPageLesson1_2_3 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson1_2_3.setText("Lesson 1-2-3");

		Composite compositeLesson1_2_3 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson1_2_3.setControl(compositeLesson1_2_3);

		styledTextXML = new StyledText(compositeLesson1_2_3, SWT.BORDER);
		styledTextXML.setBounds(10, 36, 430, 100);
		styledTextXML.setWordWrap(true);

		styledTextEvent = new StyledText(compositeLesson1_2_3, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		styledTextEvent.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledTextEvent.setTopIndex(styledTextEvent.getLineCount() - 1);
			}
		});
		styledTextEvent.setBounds(10, 168, 333, 130);
		styledTextEvent.setWordWrap(true);

		Label label = new Label(compositeLesson1_2_3, SWT.NONE);
		label.setText("XML Message");
		label.setForeground(display.getSystemColor(SWT.COLOR_LIST_SELECTION));
		label.setAlignment(SWT.CENTER);
		label.setBounds(10, 15, 427, 15);

		lblIniInterfaceLevel = new Label(compositeLesson1_2_3, SWT.NONE);
		lblIniInterfaceLevel.setLocation(446, 39);
		lblIniInterfaceLevel.setSize(95, 15);
		lblIniInterfaceLevel.setText("Ini Interface level");

		textIniInterfaceLevel = new Text(compositeLesson1_2_3, SWT.BORDER);
		textIniInterfaceLevel.setLocation(547, 36);
		textIniInterfaceLevel.setSize(45, 21);

		btnAutoConnect = new Button(compositeLesson1_2_3, SWT.NONE);
		btnAutoConnect.setLocation(598, 34);
		btnAutoConnect.setSize(95, 25);
		btnAutoConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnAutoConnect_Click(e);
			}
		});
		btnAutoConnect.setText("Auto Connect");

		Label lblXmlMessageResponse = new Label(compositeLesson1_2_3, SWT.NONE);
		lblXmlMessageResponse.setLocation(360, 147);
		lblXmlMessageResponse.setSize(187, 15);
		lblXmlMessageResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblXmlMessageResponse.setText("XML Message Response Validation");

		styledTextXMLValidate = new StyledText(compositeLesson1_2_3, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		styledTextXMLValidate.setLocation(360, 168);
		styledTextXMLValidate.setSize(333, 130);
		styledTextXMLValidate.setWordWrap(true);

		textDeviceToken = new Text(compositeLesson1_2_3, SWT.BORDER);
		textDeviceToken.setLocation(547, 85);
		textDeviceToken.setSize(146, 21);
		textDeviceToken.setEditable(false);

		lblDeviceToken = new Label(compositeLesson1_2_3, SWT.NONE);
		lblDeviceToken.setLocation(446, 88);
		lblDeviceToken.setSize(95, 15);
		lblDeviceToken.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		lblDeviceToken.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblDeviceToken.setText("Device Token");

		tabPageLesson4_5 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson4_5.setText("Lesson 4-5");

		compositeLesson4_5 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson4_5.setControl(compositeLesson4_5);

		label_1 = new Label(compositeLesson4_5, SWT.NONE);
		label_1.setText("Device List");
		label_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_1.setBounds(10, 10, 187, 15);

		Group grpDeviceAcquireRequest = new Group(compositeLesson4_5, SWT.NONE);
		grpDeviceAcquireRequest.setLocation(551, 62);
		grpDeviceAcquireRequest.setSize(142, 166);
		grpDeviceAcquireRequest.setText("Relay to");

		Label lblHostname = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblHostname.setBounds(10, 21, 55, 15);
		lblHostname.setText("hostName");

		Label lblIp = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblIp.setText("IP");
		lblIp.setBounds(10, 69, 55, 15);

		Label lblPort = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblPort.setText("Port");
		lblPort.setBounds(10, 117, 55, 15);

		textHostName = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostName.setBounds(10, 42, 122, 21);

		textHostIP = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostIP.setBounds(10, 90, 122, 21);

		textHostPort = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostPort.setBounds(10, 138, 76, 21);

		btnDeviceAcquireRequest = new Button(compositeLesson4_5, SWT.NONE);
		btnDeviceAcquireRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDeviceAcquireRequest_Click(e);
			}
		});
		btnDeviceAcquireRequest.setText("6 - Device Acq. Request");
		btnDeviceAcquireRequest.setEnabled(false);
		btnDeviceAcquireRequest.setAlignment(SWT.LEFT);
		btnDeviceAcquireRequest.setBounds(551, 31, 142, 25);

		lblNewLabel_2 = new Label(compositeLesson4_5, SWT.NONE);
		lblNewLabel_2.setForeground(SWTResourceManager.getColor(255, 165, 0));
		lblNewLabel_2.setBounds(554, 250, 139, 15);
		lblNewLabel_2.setText("Paperout - Paperjam");

		lblGreenReady = new Label(compositeLesson4_5, SWT.NONE);
		lblGreenReady.setForeground(SWTResourceManager.getColor(50, 205, 50));
		lblGreenReady.setText("Ready");
		lblGreenReady.setBounds(554, 235, 139, 15);

		lblOrangePaperjam = new Label(compositeLesson4_5, SWT.NONE);
		lblOrangePaperjam.setForeground(SWTResourceManager.getColor(255, 165, 0));
		lblOrangePaperjam.setText("diskError - init");
		lblOrangePaperjam.setBounds(554, 265, 139, 15);

		lblRed = new Label(compositeLesson4_5, SWT.NONE);
		lblRed.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblRed.setText("Offline - PowerOff");
		lblRed.setBounds(554, 280, 139, 15);

		tableDeviceAcquireList = new Table(compositeLesson4_5, SWT.BORDER | SWT.FULL_SELECTION);
		tableDeviceAcquireList.setLocation(10, 31);
		tableDeviceAcquireList.setSize(535, 262);
		tableDeviceAcquireList.setLinesVisible(true);
		tableDeviceAcquireList.setHeaderVisible(true);

		tableColumn = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn.setWidth(120);
		tableColumn.setText("DeviceName");

		tableColumn_1 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_1.setWidth(110);
		tableColumn_1.setText("HostName");

		tableColumn_2 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_2.setWidth(62);
		tableColumn_2.setText("IP");

		tableColumn_3 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_3.setWidth(48);
		tableColumn_3.setText("Port");

		tableColumn_4 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_4.setWidth(69);
		tableColumn_4.setText("Vendor");

		tableColumn_5 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_5.setWidth(55);
		tableColumn_5.setText("Model");

		tableColumn_6 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_6.setWidth(67);
		tableColumn_6.setText("Status");

		tabPageLesson6_7 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson6_7.setText("Lesson 6-7");

		Composite compositeLesson6_7 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson6_7.setControl(compositeLesson6_7);

		Label lblInterfacemoderequest = new Label(compositeLesson6_7, SWT.NONE);
		lblInterfacemoderequest
				.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblInterfacemoderequest.setBounds(536, 10, 125, 15);
		lblInterfacemoderequest.setText("interface Mode");

		label_2 = new Label(compositeLesson6_7, SWT.NONE);
		label_2.setText("Connected device List");
		label_2.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_2.setBounds(10, 10, 187, 15);

		tableConnectedDeviceList = new Table(compositeLesson6_7, SWT.BORDER | SWT.FULL_SELECTION);
		tableConnectedDeviceList.setLinesVisible(true);
		tableConnectedDeviceList.setHeaderVisible(true);
		tableConnectedDeviceList.setBounds(10, 31, 520, 262);

		tableColumn_13 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_13.setWidth(130);
		tableColumn_13.setText("DeviceName");

		tableColumn_16 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_16.setWidth(100);
		tableColumn_16.setText("IP");

		tableColumn_17 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_17.setWidth(60);
		tableColumn_17.setText("Port");

		tableColumn_18 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_18.setWidth(80);
		tableColumn_18.setText("Status");

		tblclmnMode = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tblclmnMode.setWidth(82);
		tblclmnMode.setText("Mode");

		comboInterfaceMode = new Combo(compositeLesson6_7, SWT.READ_ONLY);
		comboInterfaceMode.setBounds(536, 31, 157, 23);

		btnInterfaceModeRequest = new Button(compositeLesson6_7, SWT.NONE);
		btnInterfaceModeRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnInterfaceModeRequest_Click(e);
			}
		});
		btnInterfaceModeRequest.setText(" 7 - Interface Mode  Request");
		btnInterfaceModeRequest.setAlignment(SWT.LEFT);
		btnInterfaceModeRequest.setBounds(536, 60, 157, 25);

		btnDeviceLockRequest = new Button(compositeLesson6_7, SWT.NONE);
		btnDeviceLockRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDeviceLockRequest_Click(e);
			}
		});
		btnDeviceLockRequest.setLocation(536, 91);
		btnDeviceLockRequest.setSize(157, 25);
		btnDeviceLockRequest.setAlignment(SWT.LEFT);
		btnDeviceLockRequest.setText(" 8 - Device Lock Request");

		tabPageLesson19 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson19.setText("Lesson 19");

		compositeLesson19 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson19.setControl(compositeLesson19);

		lblApplicationStopRequest = new Label(compositeLesson19, SWT.NONE);
		lblApplicationStopRequest.setText("Application Stop Request");
		lblApplicationStopRequest.setForeground(SWTResourceManager
				.getColor(SWT.COLOR_LIST_SELECTION));
		lblApplicationStopRequest.setBounds(10, 10, 187, 15);

		tableApplicationStopCommandRequest = new Table(compositeLesson19, SWT.BORDER
				| SWT.FULL_SELECTION);
		tableApplicationStopCommandRequest.setBounds(10, 31, 683, 267);
		tableApplicationStopCommandRequest.setHeaderVisible(true);
		tableApplicationStopCommandRequest.setLinesVisible(true);

		tblclmnTime = new TableColumn(tableApplicationStopCommandRequest, SWT.NONE);
		tblclmnTime.setWidth(139);
		tblclmnTime.setText("Time");

		tblclmnMessage = new TableColumn(tableApplicationStopCommandRequest, SWT.NONE);
		tblclmnMessage.setWidth(438);
		tblclmnMessage.setText("Message");

		tblclmnCanDefer = new TableColumn(tableApplicationStopCommandRequest, SWT.NONE);
		tblclmnCanDefer.setWidth(100);
		tblclmnCanDefer.setText("Can Defer ?");

		// lesson 20

		tabPageLesson20 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson20.setText("Lesson 20");

		Composite compositeLesson20 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson20.setControl(compositeLesson20);

		btnByeRequest = new Button(compositeLesson20, SWT.NONE);
		btnByeRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnByeRequest_Click(e);
			}
		});
		btnByeRequest.setText("Bye Request");
		btnByeRequest.setBounds(496, 31, 197, 25);

		styledTextByeResponse = new StyledText(compositeLesson20, SWT.BORDER);
		styledTextByeResponse.setWordWrap(true);
		styledTextByeResponse.setBounds(10, 31, 480, 100);

		labelByeResponse = new Label(compositeLesson20, SWT.NONE);
		labelByeResponse.setText("byeResponse");
		labelByeResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		labelByeResponse.setBounds(10, 10, 100, 15);

		btnDisconnect = new Button(compositeLesson20, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDisconnect_Click(e);
			}
		});
		btnDisconnect.setText("Disconnect");
		btnDisconnect.setBounds(496, 62, 197, 25);

		// other widgets

		tableEvents = new Table(shlCupptSdk, SWT.BORDER | SWT.FULL_SELECTION);
		tableEvents.setBounds(10, 463, 711, 138);
		tableEvents.setHeaderVisible(true);
		tableEvents.setLinesVisible(true);

		tableColumn_7 = new TableColumn(tableEvents, SWT.NONE);
		tableColumn_7.setWidth(156);
		tableColumn_7.setText("Time");

		tableColumn_8 = new TableColumn(tableEvents, SWT.NONE);
		tableColumn_8.setWidth(550);
		tableColumn_8.setText("Event");

		Group grpLessonGoals = new Group(shlCupptSdk, SWT.NONE);
		grpLessonGoals.setText("Lesson Goals");
		grpLessonGoals.setBounds(727, 137, 271, 464);

		styledTextGoals = new StyledText(grpLessonGoals, SWT.BORDER);
		styledTextGoals.setBackground(SWTResourceManager.getColor(255, 255, 204));
		styledTextGoals.setBounds(10, 23, 251, 431);
		styledTextGoals.setWordWrap(true);

		/*************************************************************************************/

		loadEnvironmentVariables();
		loadGoals();
		loadIni("sample-dcs.ini");
		loadInterfaceModeList();

		// tabFolder.setSelection(4);

		sequence = 1;

		String testSequence = String.valueOf(sequence);

		while (testSequence.length() < 3)
			testSequence = "0" + testSequence;

	}

	// load Environment Variables
	private void loadEnvironmentVariables() {
		try {
			textCUPPSPN.setText(System.getenv("CUPPSPN"));
			textCUPPSPP.setText(System.getenv("CUPPSPP"));

			textCUPPSPLT.setText(System.getenv("CUPPSPLT"));
			textCUPPSCN.setText(System.getenv("CUPPSCN"));
			textCUPPSACN.setText(System.getenv("CUPPSACN"));

			textCUPPSOPR.setText(System.getenv("CUPPSOPR"));
			textCUPPSUN.setText(System.getenv("CUPPSUN"));

			textXSDU.setText(System.getenv("CUPPSXSDU"));
		} catch (Exception ec) {
			tableEventAddLine("Error getting Environment variables:" + ec);
		}

	}

	// load text goals
	private void loadGoals() {
		styledTextGoals.append("1. Receiving <applicationStopCommandRequest> message \n");
		styledTextGoals.append("\n");
		styledTextGoals
				.append("2. Sending the <applicationStopCommandResponse> message with the defer response \n");
		styledTextGoals.append("\n");
	}

	private void loadIni(String iniPath) {

		// Read from an input stream
		try {
			InputStream fs = new BufferedInputStream(new FileInputStream(iniPath));

			ProfileReader ini = new ProfileReader();
			ini.load(fs);

			InterfaceLevel = ini.getProperty("interfaceLevel", "level");

			XSDLocalPath = ini.getProperty("interfaceLevel", "XSDLocalPath");

			XSDVersion = ini.getProperty("interfaceLevel", "XSDVersion");

			textIniInterfaceLevel.setText(InterfaceLevel);

		} catch (FileNotFoundException e) {
			dialog(iniPath + " not found.", SWT.ICON_ERROR);
		} catch (Exception e) {
			tableEventAddLine("Ini File error : " + e.getMessage());
		}
	}

	private void loadInterfaceModeList() {

		comboInterfaceMode.add("aea"); // AEA mode
		comboInterfaceMode.add("Standard"); // Standard mode
		comboInterfaceMode.add("Raw"); // Raw mode
		comboInterfaceMode.add("Special"); // Special mode
		comboInterfaceMode.add("default"); // default mode => CuppT

		comboInterfaceMode.select(4);

	}

	private void btnDeviceAcquireRequest_Click(SelectionEvent e) {

		if (tableDeviceAcquireList.getSelectionCount() > 0) {

			//
			for (TableItem selectedItem : tableDeviceAcquireList.getSelection()) {

				String DeviceName = selectedItem.getText(0);
				String DeviceIP = selectedItem.getText(2);
				int DevicePort = Integer.parseInt(selectedItem.getText(3));

				DeviceToken = textDeviceToken.getText();
				connectDevice(DeviceIP, DevicePort, DeviceName);

			}

		} else {

			dialog("Please select a device", SWT.ICON_ERROR | SWT.OK);

		}
	}

	private void btnInterfaceModeRequest_Click(SelectionEvent e) {
		if (tableConnectedDeviceList.getSelectionCount() > 0) {
			if (comboInterfaceMode.getSelectionIndex() >= 0) {

				if ((InterfaceLevel.contentEquals("01.") && comboInterfaceMode.getText().equals(
						"default"))) {
					dialog("Interface Mode not supported", SWT.ICON_INFORMATION | SWT.OK);
				} else {
					//
					for (TableItem selectedItem : tableConnectedDeviceList.getSelection()) {

						String DeviceIP = selectedItem.getText(1);
						int DevicePort = Integer.parseInt(selectedItem.getText(2));

						DeviceToken = textDeviceToken.getText();

						CupptSocket socket = getCupptSocket(DeviceIP, DevicePort);

						// set the new mode
						selectedItem.setText(4, comboInterfaceMode.getText());

						// send mode request
						socket.setInterfaceMode(comboInterfaceMode.getText());
						sendXML("interfaceModeRequest", socket);

					}

				}
			} else {
				dialog("Please select a mode", SWT.ICON_ERROR | SWT.OK);
			}
		} else {
			dialog("Please select a device", SWT.ICON_ERROR | SWT.OK);
		}
	}

	private void btnDeviceLockRequest_Click(SelectionEvent e) {

		for (TableItem selectedItem : tableConnectedDeviceList.getSelection()) {

			String DeviceIP = selectedItem.getText(1);
			int DevicePort = Integer.parseInt(selectedItem.getText(2));

			DeviceToken = textDeviceToken.getText();

			CupptSocket socket = getCupptSocket(DeviceIP, DevicePort);

			if (!socket.isLocked()) {
				sendXML("deviceLockRequest", socket);
			} else {
				dialog("Device already Locked !", SWT.ICON_WARNING);
			}

			tabFolder.setSelection(3);

		}
	}

	String decodeUTF8(byte[] bytes) throws UnsupportedEncodingException {
		return new String(bytes, "UTF-8");
	}

	byte[] encodeUTF8(String string) throws UnsupportedEncodingException {
		return string.getBytes("UTF-8");
	}

	/********************************************************************/

	private void btnCuptestInstancesRequest_Click(SelectionEvent e) {

		// send message
		sendXML("cuptestInstancesRequest");
	}

	private void btnAutoConnect_Click(SelectionEvent e) {
		if (!textIniInterfaceLevel.getText().isEmpty()) {
			textIniInterfaceLevel.setEnabled(false);

			InterfaceLevel = textIniInterfaceLevel.getText();
			autoconnect = true;

			// call socket connection event
			try {
				if (!platformSocket.getSocket().isConnected()
						|| platformSocket.getSocket().isClosed()) {

					String IP = textCUPPSPN.getText();
					int port = Integer.parseInt(textCUPPSPP.getText());

					platformSocket = new CupptSocket(IP, port, "platform");

					// platformMessageID = 0xFFFF0000
					platformMessageID = "4294901760";

					tableEventAddLine("Connected to " + IP + ":" + port);
					btnCuptestInstancesRequest.setEnabled(true);

					// Init ObservableListener
					ObservableListener observablePlatformListener = new ObservableListener(
							platformSocket);

					platformSocket.setObservableListener(observablePlatformListener);

					// Add observator

					platformSocket.getObservableListener().addObservateur(new Observateur() {
						public void update(String message) {

							final String XML = message;

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {

									styledTextEventAppend('R', XML);

									tableEventAddLine("Message " + platformSocket.getIP() + ":"
											+ platformSocket.getPort() + " <-- " + XML);

									XMLReader(XML, platformSocket);
								}
							});

						}
					});

					// btnInterfacelevelsavailablerequest.setEnabled(true);

					if (autoconnect) {
						// buttonInterfacesLevelAvailableRequest_Click(null);
						sendXML("interfaceLevelsAvailableRequest");
					}

				} else {
					dialog("Already connected", SWT.ICON_WARNING);
				}
			} catch (Exception ec) {
				tableEventAddLine("Error connectiong to server:" + ec);
				disconnect();
			}

		} else {
			dialog("InterfaceLevel value not found", SWT.ICON_ERROR | SWT.OK);
		}
	}

	private void btnByeRequest_Click(SelectionEvent e) {
		// send message
		sendXML("byeRequest");

	}

	private void btnDisconnect_Click(SelectionEvent e) {
		disconnect();

		tabFolder.setSelection(0);

	}

	private void sendXML(String XML) {
		sendXML(XML, platformSocket);
	}

	// override function
	private void sendXML(String XML, CupptSocket socket) {

		generateXML(XML);
		// send message
		sendMsg(styledTextXML.getText(), socket);
	}

	private void sendMsg(String XML, CupptSocket socket) {

		try {
			if (XML.contains("{MESSAGEID}"))
				socket.incrementMessageID();

			// replace XML vars
			XML = replace(XML, "{MESSAGEID}", String.valueOf(socket.getMessageID()));
			XML = replace(XML, "{PLATFORMMESSAGEID}", platformMessageID);
			XML = replace(XML, "{XSDVersion}", XSDVersion);
			XML = replace(XML, "{INTERFACELEVEL}", InterfaceLevel);
			XML = replace(XML, "{AIRLINE}", AirlineCode);
			XML = replace(XML, "{PLATFORMDEFINEDPARAMETER}", "");

			XML = replace(XML, "{APPLICATIONCODE}", ApplicationCode);
			XML = replace(XML, "{APPLICATIONNAME}", ApplicationName);
			XML = replace(XML, "{APPLICATIONVERSION}", ApplicationVersion);
			XML = replace(XML, "{APPLICATIONDATA}", ApplicationData);

			XML = replace(XML, "{DEVICENAME}", socket.getName());
			XML = replace(XML, "{DEVICETOKEN}", DeviceToken);

			XML = replace(XML, "{HOSTNAME}", textHostName.getText());
			XML = replace(XML, "{HOSTIP}", textHostIP.getText());
			XML = replace(XML, "{HOSTPORT}", textHostPort.getText());

			XML = replace(XML, "{INTERFACEMODE}", socket.getInterfaceMode());

			XML = replace(XML, "{AEATEXT}", aeaText);
			XML = replace(XML, "{AEABINARY}", aeaBinary);

			XML = replace(XML, "{APPLICATIONSTOPCOMMANDRESPONSERESULT}",
					applicationStopCommandResponseResult);

			// send header msg
			XML = generateMsgHeader(XML) + XML;

			tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " --> " + XML);

			styledTextEventAppend('S', XML);

			socket.send(XML); // send message

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String generateMsgHeader(String XML) throws UnsupportedEncodingException {
		// send header msg
		String version = "01";
		String validCode = "00";
		String size = "" + Integer.toHexString(XML.getBytes("utf-8").length);

		while (size.length() < 6)
			size = "0" + size;

		return version + validCode + size.toUpperCase();
	}

	private void clear() {
		textIniInterfaceLevel.setEnabled(true);

		styledTextXML.setText("");
		styledTextEvent.setText("");
		styledTextXMLValidate.setText("");
		styledTextByeResponse.setText("");
		textDeviceToken.setText("");

		// tableInterfaceLevels.removeAll();
		tableDeviceAcquireList.removeAll();

		// btnInterfacelevelsavailablerequest.setEnabled(false);
		// btnInterfacelevelrequest.setEnabled(false);
		// btnUnsupportedInterfacelevelrequest.setEnabled(false);
		// btnObsoleteInterfacelevelrequest.setEnabled(false);
		// btnAuthenticateRequest.setEnabled(false);

		// btnDeviceQueryRequest.setEnabled(false);
		btnDeviceAcquireRequest.setEnabled(false);
		btnCuptestInstancesRequest.setEnabled(false);

		tableDeviceAcquireList.removeAll();
		tableConnectedDeviceList.removeAll();

	}

	/*
	 * Close the Input/Output streams and disconnect not much to do in the catch
	 * clause
	 */
	private void disconnect() {
		try {

			platformSocket.disconnect();

			// devices
			for (CupptSocket socket : deviceSocketList) {

				System.out.println("Stop " + socket);
				socket.disconnect();
			}

			clear();

		} catch (Exception e) {
			System.out.println("Exception " + e.getMessage());
		}

	}

	private int dialog(String Message, int OPTIONS) {

		// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
		// ICON_WORKING
		MessageBox dialog = new MessageBox(shlCupptSdk, OPTIONS);

		dialog.setText("Airline Application");
		dialog.setMessage(Message);
		return dialog.open();

	}

	private void tableEventAddLine(String text) {

		TableItem item = new TableItem(tableEvents, SWT.NONE);
		item.setText(0, getDate());
		item.setText(1, text);

		System.out.println(text);

		tableEvents.select(tableEvents.getItemCount() - 1);
		tableEvents.showSelection();
	}

	private void styledTextEventAppend(char T, String str) {

		String symbol = (T == 'S') ? "-->" : "<--";

		styledTextEvent.append(T + " " + symbol + " " + getDate() + " :\n");
		styledTextEvent.append(str);
		styledTextEvent.append("\n\r");
	}

	// donne la date avec un format par d�faut
	private String getDate() {
		return getDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	// surcharge de la fonction pr�c�dente avec le choix du format
	private String getDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private void generateXML(String type) {
		// header
		String XML = "";

		// body
		// si on r�pond � la plateforme, on lui renvoie le messageID
		String messageIDType = (type.contains("Response")) ? "{PLATFORMMESSAGEID}" : "{MESSAGEID}";

		XML += "<cupps messageID=\""
				+ messageIDType
				+ "\" messageName = \""
				+ type
				+ "\" xmlns=\"http://www.cupps.aero/cupps/{INTERFACELEVEL}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" >";

		if (type.equals("interfaceLevelsAvailableRequest")) {
			XML += "<interfaceLevelsAvailableRequest hsXSDVersion=\"{XSDVersion}\"/>";
		} else if (type.equals("interfaceLevelRequest")) {
			XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
		} else if (type.equals("authenticateRequest")) {
			XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

			XML += "<applicationList>";
			XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
			XML += "</applicationList>";

			XML += "</authenticateRequest>";
		} else if (type.equals("deviceQueryRequest")) {
			XML += "<deviceQueryRequest deviceName=\"{DEVICENAME}\"/>";
		} else if (type.equals("deviceAcquireRequest")) {
			XML += "<deviceAcquireRequest deviceName=\"{DEVICENAME}\" deviceToken=\"{DEVICETOKEN}\" airlineID=\"{AIRLINE}\"/>";

			if (!textHostName.getText().equals("") && !textHostIP.getText().equals("")
					&& !textHostPort.getText().equals(""))
				XML += "<relayTo hostName=\"{HOSTNAME}\" ip=\"{HOSTIP}\" port=\"{HOSTPORT}\"/>";
		} else if (type.equals("interfaceModeRequest")) {

			XML += "<interfaceModeRequest mode=\"{INTERFACEMODE}\"></interfaceModeRequest>";

		} else if (type.equals("deviceLockRequest")) {

			XML += "<deviceLockRequest renew=\"true\"></deviceLockRequest>";

		} else if (type.equals("deviceUnlockRequest")) {

			XML += "<deviceUnlockRequest></deviceUnlockRequest>";

		} else if (type.equals("aeaRequest")) {

			XML += "<aeaRequest>";

			if (!InterfaceLevel.equals("01.01"))
				XML += "<aeaMessage>";

			XML += "<aeaText><![CDATA[{AEATEXT}]]></aeaText>";

			if (AeaRequestType.equals("pcx")) {
				XML += "<aeaBinary><![CDATA[{AEABINARY}]]></aeaBinary>";
			}

			if (!InterfaceLevel.equals("01.01"))
				XML += "</aeaMessage>";

			XML += "</aeaRequest>";

		} else if (type.equals("deviceReleaseRequest")) {

			XML += "<deviceReleaseRequest></deviceReleaseRequest>";

		} else if (type.equals("cuptestInstancesRequest")) {
			XML += "<cuptestInstancesRequest />";
		} else if (type.equals("byeRequest")) {
			XML += "<byeRequest />";
		} else if (type.equals("applicationStopCommandResponse")) {

			XML += "<applicationStopCommandResponse result='{APPLICATIONSTOPCOMMANDRESPONSERESULT}'></applicationStopCommandResponse>";

		}

		// end
		XML += "</cupps>";

		styledTextXML.setText(XML);

	}

	private String replace(String originalText, String subStringToFind,
			String subStringToReplaceWith) {
		int s = 0;
		int e = 0;

		StringBuffer newText = new StringBuffer();

		while ((e = originalText.indexOf(subStringToFind, s)) >= 0) {

			newText.append(originalText.substring(s, e));
			newText.append(subStringToReplaceWith);
			s = e + subStringToFind.length();

		}

		newText.append(originalText.substring(s));
		return newText.toString();

	}

	private void XMLReader(String rmessage, CupptSocket socket) {

		int startIndex = rmessage.indexOf('<');

		if (startIndex > -1) {
			try {

				// String prefix = rmessage.substring(0, startIndex);
				// System.out.println("prefix : '" + prefix + "'");

				String XML = rmessage.substring(rmessage.indexOf('<'));

				styledTextXMLValidate.setText("");
				// parse an XML document into a DOM tree
				DocumentBuilder parser;
				parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				// Document document = parser.parse(new File("instance.xml"));
				Document document = parser.parse(new InputSource(new StringReader(XML)));
				// show parsed message
				for (int i = 0; i < document.getElementsByTagName("*").getLength(); i++) {

					Node node = document.getElementsByTagName("*").item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						styledTextXMLValidate.append(node.getNodeName() + "\n");

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								styledTextXMLValidate.append("\t" + attr.getNodeName() + ": "
										+ attr.getNodeValue() + "\n");
							}
						}
					}
				}

				Node node = document.getElementsByTagName("cupps").item(0);
				String message = node.getAttributes().getNamedItem("messageName").getNodeValue();

				// s'il s'agit d'une requete de la plateforme
				if (message.contains("Request")) {

					// on lui renvoie son messageID
					platformMessageID = document.getElementsByTagName("cupps").item(0)
							.getAttributes().getNamedItem("messageID").getNodeValue();

				}

				if (message.equals("interfaceLevelsAvailableResponse")) {

					if (autoconnect) {

						// send message
						sendXML("interfaceLevelRequest");

					}
				} else if (message.equals("interfaceLevelResponse")) {

					if (autoconnect) {

						// send message
						sendXML("authenticateRequest");

					}

				} else if (message.equals("authenticateResponse")) {

					node = document.getElementsByTagName("authenticateResponse").item(0);

					if (node.hasAttributes()) {
						textDeviceToken.setText(node.getAttributes().getNamedItem("deviceToken")
								.getNodeValue());
					}

					NodeList nodeList = document.getElementsByTagName("device");

					if (InterfaceLevel.contains("00.")) {
						System.out.println("CUPPT Interface"); // CUPPT
																// interface

						for (int i = 0; i < nodeList.getLength(); i++) {
							node = nodeList.item(i);
							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								TableItem item = new TableItem(tableDeviceAcquireList, SWT.NONE);

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
								item.setText(1, attrs.getNamedItem("hostName").getNodeValue());
								item.setText(2, attrs.getNamedItem("IP").getNodeValue());
								item.setText(3, attrs.getNamedItem("Port").getNodeValue());
								item.setText(4, attrs.getNamedItem("Vendor").getNodeValue());
								item.setText(5, attrs.getNamedItem("Model").getNodeValue());
								item.setText(6, attrs.getNamedItem("Status").getNodeValue());

							}
						}

					} else {
						System.out.println("CUPPS Interface"); // CUPPS
																// Interface

						for (int i = 0; i < nodeList.getLength(); i++) {

							TableItem item = new TableItem(tableDeviceAcquireList, SWT.NONE);

							node = nodeList.item(i);

							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());

							}

							NodeList children = node.getChildNodes();

							for (int j = 0; j < children.getLength(); j++) {
								Node child = children.item(j);

								if (child.getNodeName().contains("DeviceParameter")) {
									NodeList subchildren = child.getChildNodes();

									for (int k = 0; k < subchildren.getLength(); k++) {
										Node subChild = subchildren.item(k);

										if (subChild.getNodeName() == "ipAndPort") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(1, attrs.getNamedItem("hostName")
													.getNodeValue());
											item.setText(2, attrs.getNamedItem("ip").getNodeValue());
											item.setText(3, attrs.getNamedItem("port")
													.getNodeValue());

										}

										if (subChild.getNodeName() == "vendorModelInfo") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(4, attrs.getNamedItem("vendor")
													.getNodeValue());
											item.setText(5, attrs.getNamedItem("model")
													.getNodeValue());
										}

										if (subChild.getNodeName().contains("Status")) {
											NamedNodeMap attrs = subChild.getAttributes();

											for (int l = 0; l < attrs.getLength(); l++) {

												if (attrs.item(l).getNodeValue().equals("true")) {
													item.setText(6, attrs.item(l).getNodeName());
												}
											}
										}

									}

								}

							}
						}
					}

					// colorize all tableDeviceQueryList lines when
					// authenticateResponse happens
					colorizeDeviceStatus();

					tabFolder.setSelection(1);
					btnDeviceAcquireRequest.setEnabled(true);

				} else if (message.equals("deviceQueryResponse")) {

					String response = "deviceQueryResponse \n";

					NodeList nodeList = document.getElementsByTagName("device");
					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								response += attr.getNodeName();
								if (attr.getNodeName().length() <= 11) {
									response += "\t";
								}
								if (attr.getNodeName().length() <= 6) {
									response += "\t";
								}
								response += ": " + attr.getNodeValue() + "\n";
							}
						}
					}

					dialog(response, SWT.ICON_WORKING);

				} else if (message.equals("deviceAcquireResponse")) {

					NodeList nodeList = document.getElementsByTagName("device");

					if (InterfaceLevel.contains("00.")) {
						System.out.println("CUPPT Interface"); // CUPPT
																// interface

						for (int i = 0; i < nodeList.getLength(); i++) {
							node = nodeList.item(i);
							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								TableItem itemConnectedDevice = new TableItem(
										tableConnectedDeviceList, SWT.NONE);

								itemConnectedDevice.setText(0, attrs.getNamedItem("deviceName")
										.getNodeValue());

								itemConnectedDevice.setText(1, attrs.getNamedItem("IP")
										.getNodeValue());

								itemConnectedDevice.setText(2, attrs.getNamedItem("Port")
										.getNodeValue());

								itemConnectedDevice.setText(3, attrs.getNamedItem("Status")
										.getNodeValue());

							}
						}

					} else {
						System.out.println("CUPPS Interface"); // CUPPS
																// Interface

						for (int i = 0; i < nodeList.getLength(); i++) {

							TableItem itemConnectedDevice = new TableItem(tableConnectedDeviceList,
									SWT.NONE);

							node = nodeList.item(i);

							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								itemConnectedDevice.setText(0, attrs.getNamedItem("deviceName")
										.getNodeValue());

							}

							NodeList children = node.getChildNodes();

							for (int j = 0; j < children.getLength(); j++) {
								Node child = children.item(j);

								if (child.getNodeName() == "ipAndPort") {
									NamedNodeMap attrs = child.getAttributes();

									itemConnectedDevice.setText(1, attrs.getNamedItem("ip")
											.getNodeValue());

									itemConnectedDevice.setText(2, attrs.getNamedItem("Port")
											.getNodeValue());

								}

								if (child.getNodeName() == "Status") {
									NamedNodeMap attrs = child.getAttributes();

									for (int k = 0; k < attrs.getLength(); k++) {

										if (attrs.item(k).getNodeValue().equals("true")) {
											itemConnectedDevice.setText(3, attrs.item(k)
													.getNodeName());

										}
									}
								}
							}
						}
					}

					// colorize all tableConnectedDevice lines when
					// deviceAcquireResponse happens
					colorizeConnectedDeviceStatus();

					// change tab page
					tabFolder.setSelection(2);
					// select last acquired device
					tableConnectedDeviceList.select(tableConnectedDeviceList.getItemCount() - 1);

				} else if (message.equals("interfaceModeResponse")) {

					node = document.getElementsByTagName("interfaceModeResponse").item(0);

					if (node.hasAttributes()) {
						String result = node.getAttributes().getNamedItem("result").getNodeValue();

						if (result.equals("OK")) {

							// socket.setInterfaceMode(); //interface mode
							// already

							dialog(socket.getName() + " interfaceMode = "
									+ socket.getInterfaceMode(), SWT.ICON_INFORMATION);
						} else {

							dialog(socket.getName() + " interfaceMode = " + result, SWT.ICON_ERROR);
						}
					}

				} else if (message.equals("deviceLockResponse")) {

					node = document.getElementsByTagName("deviceLockResponse").item(0);

					if (node.hasAttributes()) {
						if (node.getAttributes().getNamedItem("result").getNodeValue().equals("OK")) {

							socket.setLock(true);

							dialog(socket.getName() + " locked", SWT.ICON_INFORMATION);
						}
					}

				} else if (message.equals("aeaResponse")) {

				} else if (message.equals("applicationStopCommandRequest")) {

					node = document.getElementsByTagName("applicationStopCommandRequest").item(0);

					if (node.hasAttributes()) {

						String stopMessage = node.getAttributes().getNamedItem("stopMessage")
								.getNodeValue();
						String canDefer = node.getAttributes().getNamedItem("canDefer")
								.getNodeValue();

						TableItem item = new TableItem(tableApplicationStopCommandRequest, SWT.NONE);

						item.setText(0, getDate());
						item.setText(1, stopMessage);
						item.setText(2, canDefer);

						String deferMessage = "Plaform message : \n '" + stopMessage
								+ "'\n  Defer the stop command ?";

						if (dialog(deferMessage, SWT.ICON_QUESTION | SWT.NO | SWT.YES) == SWT.YES) {

							applicationStopCommandResponseResult = "defer";

							sendXML("applicationStopCommandResponse");
						} else {
							applicationStopCommandResponseResult = "OK";

							sendXML("applicationStopCommandResponse");

							btnDisconnect_Click(null);
						}

					}

				} else if (message.equals("notify")) {

					NodeList children = document.getElementsByTagName("*");

					for (int j = 0; j < children.getLength(); j++) {
						Node child = children.item(j);

						if (child.getNodeName().equals("DeviceStatusNotification")) {

							if (child.hasAttributes()) {
								NamedNodeMap attrs = child.getAttributes();
								String status = attrs.getNamedItem("ddStatus").getNodeValue();

								for (int i = 0; i < tableConnectedDeviceList.getItemCount(); i++) {
									TableItem item = tableConnectedDeviceList.getItem(i);

									if (socket.getName().equals(item.getText(0))) {
										item.setText(3, status);

									}

								}
							}
						}
					}

					// colorize all tableConnectedDevice lines when
					// DeviceStatusNotification happens
					colorizeConnectedDeviceStatus();

				} else if (message.equals("cuptestInstancesResponse")) {

					String response = "cuptestInstancesResponse \n";

					NodeList nodeList = document.getElementsByTagName("instance");
					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								response += attr.getNodeName() + " : '" + attr.getNodeValue()
										+ "'\n";
							}
						}
					}

					dialog(response, SWT.ICON_WORKING);

				} else if (message.equals("byeResponse")) {

					node = document.getElementsByTagName("byeResponse").item(0);
					if (node.hasAttributes()) {
						String response = node.getAttributes().getNamedItem("result")
								.getNodeValue();

						if (response.equals("OK")) {

							btnDisconnect.setEnabled(true);

						}

					}

				}
			} catch (ParserConfigurationException e1) {

				styledTextXMLValidate.setText("ParserConfigurationException : " + e1.getMessage());

			} catch (SAXParseException spe) {

				StringBuffer sb = new StringBuffer(spe.getMessage());
				sb.append("\n\tLine number: " + spe.getLineNumber());
				sb.append("\n\tColumn number: " + spe.getColumnNumber());

				styledTextXMLValidate.setText("SAXParseException : " + sb.toString());

			} catch (SAXException e1) {

				styledTextXMLValidate.setText("SAXException : " + e1.getMessage());

			} catch (IOException eIO) {

				styledTextXMLValidate.setText("IOException : " + eIO.getMessage());

			} catch (Exception e1) {

				styledTextXMLValidate.setText("Exception when extracting XML data");

			}
		}
	}

	/*************************************************************************************************/

	private CupptSocket getCupptSocket(String dIP, int dPort) {
		CupptSocket myCupptSocket = null;
		// boolean find = false;

		for (int i = 0; i < deviceSocketList.size(); i++) {
			CupptSocket socket = deviceSocketList.get(i);

			if (dIP.equals(socket.getIP()) && dPort == (socket.getPort())) {
				myCupptSocket = socket;
			}

		}

		return myCupptSocket;
	}

	private void colorizeDeviceStatus() {

		for (int i = 0; i < tableDeviceAcquireList.getItemCount(); i++) {
			TableItem item = tableDeviceAcquireList.getItem(i);

			String status = item.getText(6).toLowerCase();
			if (status.equalsIgnoreCase("ready")) {
				item.setForeground(6, green);
			} else if (status.equalsIgnoreCase("init")) {
				item.setForeground(6, orange);
			} else {
				item.setForeground(6, red);
			}

		}
	}

	private void colorizeConnectedDeviceStatus() {

		for (int i = 0; i < tableConnectedDeviceList.getItemCount(); i++) {
			TableItem item = tableConnectedDeviceList.getItem(i);

			String status = item.getText(3).toLowerCase();

			if (status.equalsIgnoreCase("ready")) {
				item.setForeground(3, green);
			} else if (status.equalsIgnoreCase("paperOut") || status.equalsIgnoreCase("paperJam")
					|| status.equalsIgnoreCase("diskError") || status.equalsIgnoreCase("init")) {
				item.setForeground(3, orange);
			} else {
				item.setForeground(3, red);
			}

		}
	}

	private void connectDevice(String dIP, int dPort, String dName) {
		try {

			boolean connected = false;
			for (int i = 0; i < tableConnectedDeviceList.getItemCount(); i++) {
				TableItem item = tableConnectedDeviceList.getItem(i);

				if (dIP.equals(item.getText(1)) && dPort == Integer.parseInt(item.getText(2))) {
					connected = true;
					break;
				}
				System.out.println(item.getText(1) + ":" + item.getText(2));
			}

			if (connected) {
				dialog("Already connected", SWT.ICON_WARNING);
			} else {

				CupptSocket deviceSocket = new CupptSocket(dIP, dPort, dName);

				// Init ObservableListener
				final ObservableListener ObservableDeviceListener = new ObservableListener(
						deviceSocket);

				// Launch new thread
				// new Thread(ObservableDeviceListener).start();

				deviceSocket.setObservableListener(ObservableDeviceListener);

				// Add observator
				deviceSocket.getObservableListener().addObservateur(new Observateur() {
					public void update(String message) {

						final String XML = message;

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {

								CupptSocket socket = ObservableDeviceListener.getSocket();

								styledTextEventAppend('R', XML);

								tableEventAddLine("Message " + socket.getIP() + ":"
										+ socket.getPort() + " <-- " + XML);

								XMLReader(XML, socket);
							}
						});

					}
				});

				sendXML("deviceAcquireRequest", deviceSocket);

				deviceSocketList.add(deviceSocket);

			}
		} catch (Exception e) {
			tableEventAddLine("Exception : " + e.getMessage());
		}
	}

	/*************************************************************************************************/

	public byte[] readFile(File file) throws IOException {

		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		int bytes = (int) file.length();
		byte[] buffer = new byte[bytes];

		// read file
		bis.read(buffer);
		bis.close();

		return buffer;
	}

	public String readPectabFile(File file) throws IOException {

		String cleaned = "";
		BufferedReader buff = new BufferedReader(new FileReader(file));
		String line;

		while ((line = buff.readLine()) != null) {
			if (line.lastIndexOf("#") > -1) {
				// System.out.println(line.substring(0, line.contains("#")+1));
				cleaned += line.substring(0, line.lastIndexOf("#") + 1);
			} else {
				// System.out.println(line);
				// cleaned += line;
			}
		}

		// dans tous les cas, on ferme nos flux
		buff.close();

		return cleaned;

	}

}
